package biblioteca.controller;

/**
 * Created by IT on 10/05/2018.
 */
import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import jdk.nashorn.internal.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
@RequestMapping("/Library")

public class RestController {

    @Autowired
    BibliotecaCtrl ctrl;

    @RequestMapping("/world")
    public @ResponseBody
    String world() {
        return "Hello, world";
    }


    @RequestMapping("/")
    public @ResponseBody
    String fff() {
        return "Hello fff";
    }

    @CrossOrigin
    @RequestMapping(value = "/books", method = RequestMethod.POST)
    public ResponseEntity<?> getBooksByYear(@RequestParam("year") String year) {
        final List<Carte> found;
        try {
            found = ctrl.getCartiOrdonateDinAnul(year);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(JSONParser.quote("Year must be an integer value!"), HttpStatus.BAD_REQUEST);
        }

        if (!found.isEmpty()) {
            Carte[] books = new Carte[found.size()];
            int i = 0;
            for (Carte carte : found) {
                books[i] = carte;
                i++;
            }

            return ResponseEntity.ok(books);
        }

        return new ResponseEntity<>(JSONParser.quote("No Book found"), HttpStatus.NO_CONTENT);
    }
}

