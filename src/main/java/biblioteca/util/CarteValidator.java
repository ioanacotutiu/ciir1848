package biblioteca.util;

import biblioteca.model.Carte;

import java.util.Calendar;

public class CarteValidator {
	
	public static boolean isSearchStringOK(String s) throws Exception{
		boolean flag = s.matches("[a-zA-Z]+");
		if(flag == false)
			throw new Exception("String invalid");
		return flag;
	}
	
	public static void validateCarte(Carte c)throws Exception{
		if(c.getCuvinteCheie()==null){
			throw new Exception("Lista cuvinte cheie vida!");
		}
		if(c.getReferenti()==null || c.getReferenti().size()==0){
			throw new Exception("Lista autori vida!");
		}
		if (c.getReferenti().size()>35){
            throw new Exception("Numar de autori invalid!");
        }
		if(!isOKString(c.getTitlu()))
			throw new Exception("Titlu invalid!");
		if(!isOKString(c.getEditura()))
			throw new Exception("Editura invalida!");
		for(String s:c.getReferenti()){
			if(!isOKString(s))
				throw new Exception("Autor invalid!");
		}
		if (c.getCuvinteCheie()==null || c.getCuvinteCheie().size()==0){
            throw new Exception("Lista cuvinte cheie vida!");
        }
        if (c.getCuvinteCheie().size()>35){
            throw new Exception("Numar de cuvinte cheie invalid!");
        }
		for(String s:c.getCuvinteCheie()){
			if(!isOKString(s))
				throw new Exception("Cuvant cheie invalid!");
		}
		if(!CarteValidator.isNumber(c.getAnAparitie()))
			throw new Exception("An aparitie invalid!");
	}
	
	public static boolean isNumber(String s){
		if (!s.matches("[1-9][0-9]+")){
			return false;
		}
		long an=Long.parseLong(s);
		if (an<1900 || an>(Calendar.getInstance().get(Calendar.YEAR))){
			return false;
		}
		return true;
	}
	
	public static boolean isOKString(String s){
		String []t = s.split(" ");
		if(t.length==2){
			boolean ok1 = t[0].matches("[a-zA-Z]+");
			boolean ok2 = t[1].matches("[a-zA-Z]+");
			if(ok1==ok2 && ok1==true){
				return true;
			}
			return false;
		}
		return s.matches("[a-zA-Z]+");
	}
	
}
