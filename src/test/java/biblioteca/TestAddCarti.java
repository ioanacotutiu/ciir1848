package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by IT on 22/03/2018.
 */
public class TestAddCarti{
    private Carte carte=new Carte();
    private String testId;
    private String textfile="testsFile.txt";
    private String status;
    private BibliotecaCtrl bibliotecaCtrl=new BibliotecaCtrl(new CartiRepo(textfile));
    private List<String> referenti=new ArrayList<String>(Arrays.asList("autor A","autorB","autorC"));
    private List<String> cuv_Cheie=new ArrayList<String>(Arrays.asList("cuv A","cuvB","cuv C"));


    @BeforeClass
    public static void printTestId(){
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("testsFile.txt",true));
            bw.newLine();
            bw.write("TC1_ECP");
            bw.newLine();
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Before
    public void setUp() throws Exception {


        this.carte.setTitlu("Titlu carte");
        this.carte.setReferenti(this.referenti);
        this.carte.setAnAparitie("2010");
        this.carte.setEditura("editura");
        this.carte.setCuvinteCheie(this.cuv_Cheie);

    }

    @After
    public void tearDown() throws Exception {
            BufferedWriter bw = null;
            try {
                bw = new BufferedWriter(new FileWriter(this.textfile,true));
                bw.write("Status:"+status);
                bw.newLine();
                bw.newLine();
                bw.write(this.testId);
                bw.newLine();

                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    @Test
    public void TC1_ECP(){
        try{
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:record added";
        }catch (Exception ex){
            //System.out.println(ex.getMessage());
            assertTrue(false);
            status="Test failed:"+ex.getMessage();
        }
        this.testId="TC2_ECP";
        //System.out.println(this.testId);

    }

    @Test
    public void TC2_ECP(){
        try{
            carte.setTitlu("");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(false);
            status="Test failed:record added";
        }catch (Exception ex){
            //System.out.println(ex.getMessage());
            assertTrue(true);
            status="Test passed:"+ex.getMessage();
        }
        this.testId="TC3_ECP";

    }

    @Test
    public void TC3_ECP(){
        try{
            carte.getReferenti().set(0,"123abc");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(false);
            status="Test failed:record added";
        }catch (Exception ex){
            //System.out.println(ex.getMessage());
            assertTrue(true);
            status="Test passed:"+ex.getMessage();
        }
        this.testId="TC4_ECP";
    }

    @Test
    public void TC4_ECP(){
        try{
            carte.setAnAparitie("20c");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(false);
            status="Test failed:record added";
        }catch (Exception ex){
            //System.out.println(ex.getMessage());
            assertTrue(true);
            status="Test passed:"+ex.getMessage();
        }
        this.testId="TC5_ECP";
    }

    @Test
    public void TC5_ECP(){
        try{
            this.carte.setAnAparitie("1899");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(false);
            status="Test failed:record added";
        }catch (Exception ex){
            //System.out.println(ex.getMessage());
            assertTrue(true);
            status="Test passed:"+ex.getMessage();
        }
        this.testId="TC6_ECP";
    }

    @Test
    public void TC6_ECP(){
        try{
            this.carte.setAnAparitie(""+ (Calendar.getInstance().get(Calendar.YEAR)+1));
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(false);
            status="Test failed:record added";
        }catch (Exception ex){
            //System.out.println(ex.getMessage());
            assertTrue(true);
            status="Test passed:"+ex.getMessage();
        }
        this.testId="TC7_ECP";
    }

    @Test
    public void TC7_ECP(){
        try{
            this.carte.setEditura("1ab");
            bibliotecaCtrl.adaugaCarte(carte);
            status="Test failed:record added";
        }catch (Exception ex){
            //System.out.println(ex.getMessage());
            assertTrue(true);
            status="Test passed:"+ex.getMessage();
        }
        this.testId="TC8_ECP";
    }

    @Test
    public void TC8_ECP(){
        try{
            this.carte.getCuvinteCheie().set(0,"cde1");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(false);
            status="Test failed:record added";
        }catch (Exception ex){
            //System.out.println(ex.getMessage());
            assertTrue(true);
            status="Test passed:"+ex.getMessage();
        }
        this.testId="TC9_ECP";
    }

    @Test
    public void TC9_ECP(){
        try{
            this.carte.getReferenti().clear();
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(false);
            status="Test failed:record added";
        }catch (Exception ex){
            //System.out.println(ex.getMessage());
            assertTrue(true);
            status="Test passed:"+ex.getMessage();
        }
        this.testId="TC10_ECP";
    }

    @Test
    public void TC10_ECP(){
        try{
            this.carte.getCuvinteCheie().clear();
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(false);
            status="Test failed:record added";
        }catch (Exception ex){
            //System.out.println(ex.getMessage());
            assertTrue(true);
            status="Test passed:"+ex.getMessage();
        }
        this.testId="TC11_ECP";
    }

    @Test
    public void TC11_ECP(){
        try{
            this.carte.setTitlu("*abc");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(false);
            status="Test failed:record added";
        }catch (Exception ex){
            //System.out.println(ex.getMessage());
            assertTrue(true);
            status="Test passed:"+ex.getMessage();
        }
        this.testId="TC2_BVA";
    }

    //-----------------------------------------  BVA --------------------------------------------

    @Test
    public void TC2_BVA(){
        try{
            this.carte.setTitlu("Mn");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC3_BVA";
    }

    @Test
    public void TC3_BVA(){
        try{
            this.carte.setTitlu("M");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC4_BVA";
    }

    @Test
    public void TC4_BVA(){
        String titlu="";
        for (int i=0;i<255;i++){
            titlu+="a";
        }
        try{
            this.carte.setTitlu(titlu);
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC5_BVA";
    }

    @Test
    public void TC5_BVA(){
        String titlu="";
        for (int i=0;i<254;i++){
            titlu+="a";
        }
        try{
            this.carte.setTitlu(titlu);
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC9_BVA";
    }

    @Test
    public void TC9_BVA(){
        try{
            this.carte.getReferenti().remove(0);
            this.carte.getReferenti().remove(0);
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC10_BVA";
    }

    @Test
    public void TC10_BVA(){
        for (int i=0;i<31;i++){
            this.carte.getReferenti().add("abc");
        }
        try{
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC11_BVA";
    }

    @Test
    public void TC11_BVA(){
        for (int i=0;i<32;i++){
            this.carte.getReferenti().add("abc");
        }
        try{
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC12_BVA";
    }

    @Test
    public void TC12_BVA(){
        for (int i=0;i<33;i++){
            this.carte.getReferenti().add("abc");
        }
        try{
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(false);
            status="Test failed:recod added";
        }catch (Exception e){
            assertTrue(true);
            status="Test passed:"+e.getMessage();
        }
        this.testId="TC14_BVA";
    }

    @Test
    public void TC14_BVA(){
        try{
            this.carte.getReferenti().add("Mn");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC15_BVA";
    }

    @Test
    public void TC15_BVA(){
        try{
            this.carte.getReferenti().add("M");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC16_BVA";
    }

    @Test
    public void TC16_BVA(){
        String autor="";
        for (int i=0;i<255;i++){
            autor+="a";
        }
        try{
            this.carte.getReferenti().add(autor);
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC17_BVA";
    }

    @Test
    public void TC17_BVA(){
        String autor="";
        for (int i=0;i<254;i++){
            autor+="a";
        }
        try{
            this.carte.getReferenti().add(autor);
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC19_BVA";
    }

    @Test
    public void TC19_BVA(){
        try{
            this.carte.setAnAparitie("1900");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC21_BVA";
    }

    @Test
    public void TC21_BVA(){
        try{
            this.carte.setAnAparitie("1901");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC22_BVA";
    }

    @Test
    public void TC22_BVA(){
        try{
            this.carte.setAnAparitie(""+(Calendar.getInstance().get(Calendar.YEAR)-1));
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC24_BVA";
    }

    @Test
    public void TC24_BVA(){
        try{
            this.carte.setAnAparitie(""+(Calendar.getInstance().get(Calendar.YEAR)));
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC26_BVA";
    }

    @Test
    public void TC26_BVA(){
        try{
            this.carte.setEditura("Mn");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC27_BVA";
    }

    @Test
    public void TC27_BVA(){
        try{
            this.carte.setEditura("M");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC28_BVA";
    }

    @Test
    public void TC28_BVA(){
        String titlu="";
        for (int i=0;i<255;i++){
            titlu+="a";
        }
        try{
            this.carte.setEditura(titlu);
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC29_BVA";
    }

    @Test
    public void TC29_BVA(){
        String titlu="";
        for (int i=0;i<254;i++){
            titlu+="a";
        }
        try{
            this.carte.setEditura(titlu);
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC33_BVA";
    }

    @Test
    public void TC33_BVA(){
        try{
            this.carte.getCuvinteCheie().remove(0);
            this.carte.getCuvinteCheie().remove(0);
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC34_BVA";
    }

    @Test
    public void TC34_BVA(){
        for (int i=0;i<31;i++){
            this.carte.getCuvinteCheie().add("abc");
        }
        try{
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC35_BVA";
    }

    @Test
    public void TC35_BVA(){
        for (int i=0;i<32;i++){
            this.carte.getCuvinteCheie().add("abc");
        }
        try{
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC36_BVA";
    }

    @Test
    public void TC36_BVA(){
        for (int i=0;i<33;i++){
            this.carte.getCuvinteCheie().add("abc");
        }
        try{
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(false);
            status="Test failed:recod added";
        }catch (Exception e){
            assertTrue(true);
            status="Test passed:"+e.getMessage();
        }
        this.testId="TC38_BVA";
    }

    @Test
    public void TC38_BVA(){
        try{
            this.carte.getCuvinteCheie().add("Mn");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC39_BVA";
    }

    @Test
    public void TC39_BVA(){
        try{
            this.carte.getCuvinteCheie().add("M");
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC40_BVA";
    }

    @Test
    public void TC40_BVA(){
        String autor="";
        for (int i=0;i<255;i++){
            autor+="a";
        }
        try{
            this.carte.getCuvinteCheie().add(autor);
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="TC41_BVA";
    }

    @Test
    public void TC41_BVA(){
        String autor="";
        for (int i=0;i<254;i++){
            autor+="a";
        }
        try{
            this.carte.getCuvinteCheie().add(autor);
            bibliotecaCtrl.adaugaCarte(carte);
            assertTrue(true);
            status="Test passed:recod added";
        }catch (Exception e){
            assertTrue(false);
            status="Test failed:"+e.getMessage();
        }
        this.testId="";
    }

}
