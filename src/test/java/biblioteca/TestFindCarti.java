package biblioteca;

import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;
import java.util.*;
import static  org.junit.Assert.assertTrue;

/**
 * Created by IT on 01/04/2018.
 */
public class TestFindCarti {
    private CartiRepo cartiRepo=new CartiRepo("TestFileFind.txt");
    private static Map<Integer,List<Carte>> testsSetUp=new HashMap<Integer, List<Carte>>();
    private int key;

    @BeforeClass
    public static void addData(){
        testsSetUp.put(2,new ArrayList<Carte>(Arrays.asList(new Carte("t",new ArrayList<String>(Arrays.asList("a")),"1900",new ArrayList<String>(Arrays.asList("d")),"ab"))));
        testsSetUp.put(3,new ArrayList<Carte>(Arrays.asList(new Carte("t",new ArrayList<String>(Arrays.asList("Eminescu","Caragiale")),"1900",new ArrayList<String>(Arrays.asList("d")),"ab"))));
        testsSetUp.put(4,new ArrayList<Carte>());
        testsSetUp.put(5,new ArrayList<Carte>(Arrays.asList(
                new Carte("t",new ArrayList<String>(Arrays.asList("Caragiale")),"1900",new ArrayList<String>(Arrays.asList("d")),"ab"),
                new Carte("t",new ArrayList<String>(Arrays.asList("Alecsandri")),"1900",new ArrayList<String>(Arrays.asList("d")),"ab")
                )));
        testsSetUp.put(7,new ArrayList<Carte>(Arrays.asList(new Carte("t",new ArrayList<String>(Arrays.asList("a")),"1900",new ArrayList<String>(Arrays.asList("d")),"ab"))));
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("TestFileFind.txt"));
            for (Carte carte : testsSetUp.get(2)){
                writer.write(carte.toString());
                writer.newLine();
            }
            writer.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @After
    public void deleteData(){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("TestFileFind.txt"));
            if (key != -1) {
                for (Carte carte : testsSetUp.get(key)) {
                    writer.write(carte.toString());
                    writer.newLine();
                }
            }
                writer.close();
            }catch(IOException e){
                e.printStackTrace();
            }

    }

    @Test
    public void TC02(){
        assertTrue(cartiRepo.cautaCarte("abc").size()==0);
        key=3;
    }

    @Test
    public void TC03(){
        List<Carte> carti=cartiRepo.cautaCarte("escu");
        assertTrue(carti.size()==1);
        assertTrue(carti.get(0).getReferenti().get(0).toLowerCase().contains("escu"));
        key=4;
    }

    @Test
    public void TC04(){
        assertTrue(cartiRepo.cautaCarte("abc").size()==0);
        key=5;
    }

    @Test
    public void TC05(){
        List<Carte> carti=cartiRepo.cautaCarte("ale");
        assertTrue(carti.size()==2);
        assertTrue(carti.get(0).getReferenti().get(0).toLowerCase().contains("ale"));
        assertTrue(carti.get(1).getReferenti().get(0).toLowerCase().contains("ale"));
        key=7;
    }

    @Test
    public void TC07(){
        try {
           cartiRepo.cautaCarte(null);
            assertTrue(false);
        }catch (NullPointerException ex){
            assertTrue(true);
        }
        key=-1;
    }
}
