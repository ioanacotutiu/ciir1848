package biblioteca.IntegrationTesting;

import biblioteca.TestAddCarti;
import biblioteca.TestFilterYear;
import biblioteca.TestFindCarti;
import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by IT on 22/04/2018.
 */
public class TopDownTest {
    private BibliotecaCtrl bibliotecaCtrl=new BibliotecaCtrl(new CartiRepo());
    private BibliotecaCtrl mockBibliotecaCtrl=new BibliotecaCtrl(new CartiRepoMock());

    @Test
    public void testA(){
        Result result = JUnitCore.runClasses(TestAddCarti.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());
    }

    @Test
    public void testItegrateB(){
        //unit test for module B
        Result result = JUnitCore.runClasses(TestFindCarti.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());

        //integration test for module B
        //stub for C
        try{
           bibliotecaCtrl.adaugaCarte(new Carte("AAA",
                   new ArrayList<String>(Arrays.asList(
                           "Caragiale","Creanga")),
                   "1948",
                   new ArrayList<String>(Arrays.asList(
                           "basme","schite")),
                   "Albatros"));
            assertTrue(bibliotecaCtrl.cautaCarte("ale").size()==6);
            assertTrue(mockBibliotecaCtrl.getCartiOrdonateDinAnul("1948").size()==3);
            assertTrue(true);
        }catch (Exception ex){
            assertTrue(false);
        }

        deleteAddedData();
    }

    @Test
    public void IntegrareC(){
        //unit test for module C
        Result result = JUnitCore.runClasses(TestFilterYear.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());

        //integration test for module C
        //no stubs
        try{
            bibliotecaCtrl.adaugaCarte(new Carte("AAA",
                    new ArrayList<String>(Arrays.asList(
                            "Caragiale","Creanga")),
                    "1948",
                    new ArrayList<String>(Arrays.asList(
                            "basme","schite")),
                    "Albatros"));
            assertTrue(bibliotecaCtrl.cautaCarte("ale").size()==6);
            assertTrue(bibliotecaCtrl.getCartiOrdonateDinAnul("1948").size()==5);
            assertTrue(true);
        }catch (Exception ex){
            assertTrue(false);
        }

        deleteAddedData();
    }



    public void deleteAddedData(){
        List<Carte> carti=null;
        try {
            carti = bibliotecaCtrl.getCarti();

        }catch(Exception ex){
            ex.printStackTrace();
        }

        carti.remove(carti.size()-1);
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("cartiBD.dat"));
            for (Carte c:carti) {
                bw.write(c.toString());
                bw.newLine();
            }

            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
