package biblioteca.IntegrationTesting;

import biblioteca.TestAddCarti;
import biblioteca.TestFilterYear;
import biblioteca.TestFindCarti;
import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repo.CartiRepo;

import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by IT on 21/04/2018.
 */

public class BigBangTest {

    @Test
    public void TestA(){
        Result result = JUnitCore.runClasses(TestAddCarti.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());
    }

    @Test
    public void TestB(){
        Result result = JUnitCore.runClasses(TestFindCarti.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());
    }

    @Test
    public void TestC(){
        Result result = JUnitCore.runClasses(TestFilterYear.class);

        for (Failure failure : result.getFailures()) {
            System.out.println(failure.toString());
        }

        System.out.println(result.wasSuccessful());
    }

    @Test
    public void TestP_A_B_C(){
        BibliotecaCtrl bibliotecaCtrl=new BibliotecaCtrl(new CartiRepo());
        try {
            bibliotecaCtrl.adaugaCarte(new Carte("AAA",
                    new ArrayList<String>(Arrays.asList(
                            "Caragiale","Creanga")),
                            "1948",
                            new ArrayList<String>(Arrays.asList(
                            "basme","schite")),
                    "Albatros"));
            assertTrue(true);
        }catch (Exception ex){
            assertTrue(false);
        }
        try {
            assertTrue(bibliotecaCtrl.cautaCarte("ale").size()==6);
        }catch (Exception ex){
            assertTrue(false);
        }
        try {
            assertTrue(bibliotecaCtrl.getCartiOrdonateDinAnul("1948").size()==5);
        }catch (Exception ex){
            assertTrue(false);
        }
    }

    @AfterClass
    public static void deleteAddedData(){
        BibliotecaCtrl bibliotecaCtrl=new BibliotecaCtrl(new CartiRepo());
        List<Carte> carti=null;
        try {
            carti = bibliotecaCtrl.getCarti();

        }catch(Exception ex){
            ex.printStackTrace();
        }

        carti.remove(carti.size()-1);
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("cartiBD.dat"));
            for (Carte c:carti) {
                bw.write(c.toString());
                bw.newLine();
            }

            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
